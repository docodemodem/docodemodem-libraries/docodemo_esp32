# docodemo_esp32
サーキットデザイン製プログラマブル無線ユニット「どこでもでむ」用のライブラリです。


platformioで新プロジェクトを作るときは"Espressif ESP32 Dev Module"を選択してください。


## Know-How

_●SPIFFSへのデータ転送の方法_

HTMLなどをSDカードにいれて読みだすことも可能ですが、SPIFFSから読み出すことも可能です。
dataフォルダを作成し、そこに転送したいファイルを保存します。
platformio.iniに下記設定を追加し、シリアルターミナルを閉じておきます。

```
[platformio]
data_dir    = ./data 
```

次のターミナルを開いてコマンドを実行すると転送されます。

`platformio run --target uploadfs`

_●fatal error: **.h: No such file or directory と出てコンパイルできないときの対処方法_

main.cppに対象のヘッダファイルを追加してみてください。

```
#include <FS.h>
#include <WiFi.h>
```
