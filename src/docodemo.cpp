/*
 * Library for DocodeModem
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */

#include "docodemo.h"

#include "rx8130.h"
#include "SparkFunBME280.h"
#include "adp5589.h"
#include "ad5693.h"

#ifdef OLD_DM
#include "mcp3425.h"
MCP3425 adcDm = MCP3425();
#else
#include "mcp3221.h"
MCP3221 adcDm = MCP3221();
#endif

#ifndef NO_SENSOR
RX8130 rtcDm = RX8130();
BME280 SensorDm = BME280();
#endif
ADP5589 exGpioDm = ADP5589();
AD5693 dacDm = AD5693();

SemaphoreHandle_t xSemaphoreDocodemo = NULL;
const TickType_t xTicksToWait = 500UL;

TwoWire I2C_internal = TwoWire(I2C_INTERNAL_NO);

DOCODEMO::DOCODEMO()
{
}

bool DOCODEMO::begin()
{
    xSemaphoreDocodemo = xSemaphoreCreateBinary();
    configASSERT(xSemaphoreDocodemo);
    xSemaphoreGive(xSemaphoreDocodemo);

    // for LTE-M
    pinMode(RF_CTS_IN, INPUT);
#ifndef OLD_DM
    pinMode(RF_WAKE_INT_IN, INPUT);
#endif
#ifdef USE_LTE_M_CTS
    digitalWrite(RF_WAKEUP_OUT, HIGH);
    pinMode(RF_WAKEUP_OUT, OUTPUT); // for LTE-M
#ifndef DM_V4
    digitalWrite(RF_RESET_OUT, HIGH);
    pinMode(RF_RESET_OUT, OUTPUT); // for LTE-M
#endif
#else
    pinMode(RF_WAKEUP_OUT, INPUT);
#endif

#ifndef OLD_DM
    digitalWrite(EX_POWER_PORT, OFF);
    pinMode(EX_POWER_PORT, OUTPUT);
#endif

    // Start internal I2C
    I2C_internal.begin(INTERNAL_SDA, INTERNAL_SCL, (uint32_t)100000); // 100kHz frequency

    // Internal ExGPIO
    bool status = exGPIO_init(I2C_internal);
    if (!status)
    {
        Serial.println("Could not find a valid exGPIO, check wiring!");
        return false;
    }

#ifndef NO_SENSOR
    // BME280
    SensorDm.setI2CAddress(BME280_I2C_ADD);
    if (SensorDm.beginI2C(I2C_internal) == false)
    {
        Serial.println("SensorDm connect failed");
        return false;
    }
#endif

    // ADC
#ifdef OLD_DM
    adcDm.begin(MCP3425_I2C_ADD, I2C_internal);
#else
#ifdef DM_V4
    adcDm.begin(MCP3221_I2C_ADD, I2C_internal);
#else
    adcDm.begin(MCP3221_I2C_ADD, I2C_internal, 3.3F);
#endif
#endif

// DAC
#if defined(DM_V4) || defined(OLD_DM)
    dacDm.begin(AD5693_I2C_ADD, I2C_internal);
#else
    dacDm.begin(AD5693_I2C_ADD, I2C_internal, 3.3F);
#endif

#ifndef NO_SENSOR
    // RTC
    status = rtcDm.begin(RX8130_I2C_ADD, I2C_internal);
    if (status == false)
    {
        Serial.println("Could not find a valid rtc8130, check wiring!");
        return false;
    }
    else
    {
        rtcValidAtBoot = rtcDm._valid;
        if (rtcDm._valid)
        {
            // Serial.println("RTC is OK!");
        }
        else
        {
            Serial.println("rtc8130 has initialized...");
        }
    }
#endif

    return true;
}

bool DOCODEMO::LedCtrl(uint8_t led, uint8_t onoff)
{
    bool rv = false;
    if ((led != RED_LED && led != GREEN_LED) || onoff > 1)
        return rv;

    // !!! ON/HIGH ->turn off, OFF/LOW-> turn on , so invert it.
    onoff = (onoff == 1) ? 0 : 1;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        exGpioDm.gpioWrite(led, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

bool DOCODEMO::ModemPowerCtrl(uint8_t onoff)
{
    bool rv = false;
    if (onoff != ON && onoff != OFF)
        return rv;
    // !!! ON/HIGH ->turn on, OFF/LOW-> turn off
    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        exGpioDm.gpioWrite(MODEM_POWER_PORT, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

bool DOCODEMO::GpsPowerCtrl(uint8_t onoff)
{
    bool rv = false;
    if (onoff != ON && onoff != OFF)
        return rv;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        exGpioDm.gpioWrite(GPS_POWER_PORT, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

bool DOCODEMO::BeepVolumeCtrl(uint8_t vol)
{
    bool rv = false;
    if (vol > 3)
        return rv;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        exGpioDm.gpioWrite(VOLUME_EN1, vol & 0x1);  // EN1. C4
        exGpioDm.gpioWrite(VOLUME_EN2, (vol >> 1)); // EN2. C3
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

uint8_t DOCODEMO::readDipSW()
{
    uint8_t val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = exGpioDm.readRegister(0x17);
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}

#ifndef NO_SENSOR
float DOCODEMO::readPressure()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = SensorDm.readFloatPressure();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}
float DOCODEMO::readHumidity()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = SensorDm.readFloatHumidity();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}
float DOCODEMO::readTemperature()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = SensorDm.readTempC();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return val;
}
#endif

float DOCODEMO::readExADC()
{
    float val = 0;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        val = adcDm.readADC();
    }
    xSemaphoreGive(xSemaphoreDocodemo);
    return val;
}

bool DOCODEMO::setDAC(float volt)
{
    bool rv = false;
    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        dacDm.setDAC(volt);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

bool DOCODEMO::DacDown(void)
{
    bool rv = false;
    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        dacDm.Stop();
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

#ifndef NO_SENSOR
DateTime DOCODEMO::rtcNow()
{
    DateTime now = DateTime(0, 0, 0);

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        now = rtcDm.now();
        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return now;
}

bool DOCODEMO::rtcAdjust(const DateTime &dt)
{
    bool rv = false;
    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.adjust(dt);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
        rtcAdjusted = true;
    }
    return rv;
}
#endif

// Max 500kHz, but it seems 510kHz
//  500000 / val = Hz
bool DOCODEMO::setPwm(uint16_t val)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        if (val != 0)
        {
            long _val = 500000 / val;
            exGpioDm.writeRegister(ADP5589_ADR_PWM_OFFT_LOW, _val & 0xff);
            exGpioDm.writeRegister(ADP5589_ADR_PWM_OFFT_HIGH, _val >> 8);
            exGpioDm.writeRegister(ADP5589_ADR_PWM_ONT_LOW, _val & 0xff);
            exGpioDm.writeRegister(ADP5589_ADR_PWM_ONT_HIGH, _val >> 8);
            exGpioDm.writeRegister(ADP5589_ADR_PWM_CFG, 0x01);
        }
        else
        {
            exGpioDm.writeRegister(ADP5589_ADR_PWM_CFG, 0);
        }

        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

// port is 0 or 1. if error then return -1.
int DOCODEMO::readExIN(uint8_t port)
{
    int rv = -1;
    if (port > 1)
        return rv;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        switch (port)
        {
        case 0:
            rv = exGpioDm.gpioRead(EXGPIO_PORT_INPUT_0);
            break;
        case 1:
            rv = exGpioDm.gpioRead(EXGPIO_PORT_INPUT_1);
            break;
        default:
            break;
        }
#ifndef OLD_DM
        rv = (rv == 1) ? 0 : 1;
#endif

        xSemaphoreGive(xSemaphoreDocodemo);
    }
    return rv;
}

// port is 0 or 1. if error then return false.
bool DOCODEMO::exOutCtrl(uint8_t port, uint8_t val)
{
    bool rv = false;
    if (port > 1 || val > 1)
        return rv;

    // Invert
    val = (val == 1) ? 0 : 1;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        switch (port)
        {
        case 0:
            exGpioDm.gpioWrite(EXGPIO_PORT_OUTPUT_0, val);
            break;
        case 1:
            exGpioDm.gpioWrite(EXGPIO_PORT_OUTPUT_1, val);
            break;
        default:
            break;
        }
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

#ifndef OLD_DM
bool DOCODEMO::exPowerCtrl(uint8_t onoff)
{
    bool rv = false;

    if (onoff != ON && onoff != OFF)
    {
        return rv;
    }

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        digitalWrite(EX_POWER_PORT, onoff);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}
#endif

bool DOCODEMO::exGPIO_init(TwoWire &theWire)
{
    bool rv = false;
    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        // set up the exGpioDm
        bool status = exGpioDm.begin(ADP5589_I2C_ADD, theWire);
        if (status == false)
        {
            xSemaphoreGive(xSemaphoreDocodemo);
            printf("GPIO init error\n");
            return false;
        }

        //    R 0,1,2,3,4,5,6,7
        // GPIO 1,2,3,4,5,6,7,8

        //   C 0, 1, 2, 3, 4, 5, 6, 7, 8, 9,10
        // GPIO 9,10,11,12,13,14,15,16,17,18,19

        // set GPIO pins as outputs and do a thing to show we're on
        for (int i = 1; i <= 7; i++)
        {
            exGpioDm.gpioWrite(i, LOW);
            exGpioDm.gpioSetDirection(i, ADP5589_OUTPUT);
        }
        for (int i = 9; i <= 15; i++)
        {
            exGpioDm.gpioSetDirection(i, ADP5589_INPUT);
        }

        // ExGPIO
        exGpioDm.gpioSetDirection(EXGPIO_PORT_INPUT_0, ADP5589_INPUT); // input 0
        exGpioDm.gpioSetDirection(EXGPIO_PORT_INPUT_1, ADP5589_INPUT); // input 1

        exGpioDm.gpioWrite(EXGPIO_PORT_OUTPUT_0, HIGH);
        exGpioDm.gpioWrite(EXGPIO_PORT_OUTPUT_1, HIGH);
        exGpioDm.gpioSetDirection(EXGPIO_PORT_OUTPUT_0, ADP5589_OUTPUT); // output 0
        exGpioDm.gpioSetDirection(EXGPIO_PORT_OUTPUT_1, ADP5589_OUTPUT); // output 1

        /*Check register
    for (int i = 0; i <= 0x4e; i++)
    {
        uint8_t val = exGpioDm.readRegister(i);
        Serial.print(i, HEX);
        Serial.print(":");
        Serial.println(val, HEX);
    }*/
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }
    return rv;
}

#ifndef NO_SENSOR
bool DOCODEMO::setRtcTimer(const RtcTimerPeriod period, const uint16_t count)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {

        rtcDm.setTimer(period, count);
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::stopRtcTimer(void)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rtcDm.stopTimer();
        xSemaphoreGive(xSemaphoreDocodemo);
        rv = true;
    }

    return rv;
}

bool DOCODEMO::chkRtcBattery(void)
{
    bool rv = false;

    if (xSemaphoreTake(xSemaphoreDocodemo, xTicksToWait) == pdTRUE)
    {
        rv = rtcDm.chkBattery();
        xSemaphoreGive(xSemaphoreDocodemo);
    }

    return rv;
}
#endif