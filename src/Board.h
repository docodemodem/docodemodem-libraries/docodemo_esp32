/*
 * Include header for DocodeModem depend on hardware
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#ifndef _BOARD_H_
#define _BOARD_H_

/*======================================================================
 *  Include File 
 *====================================================================*/
#include <Arduino.h>
#include <Wire.h>

/*======================================================================
 *	#define for Global
 *====================================================================*/
#define _DOCODE_MODEM 1

//GPIO Expander Number
#define RED_LED 1
#define GREEN_LED 2
#define VOLUME_EN2 3
#define VOLUME_EN1 5
#define GPS_POWER_PORT 6
#define MODEM_POWER_PORT 7
#define EXGPIO_PORT_INPUT_0 8
#define EXGPIO_PORT_INPUT_1 17
#define EXGPIO_PORT_OUTPUT_0 18
#define EXGPIO_PORT_OUTPUT_1 19

//Arduino Port Number
#define MODEM_UART_RX_PORT (34) //To TX of modeule
#define MODEM_UART_TX_PORT (17) //To Rx of modeule
#define EX_UART_TX_PORT (32)
#define EX_UART_RX_PORT (36)

#define INTERNAL_SDA (21)
#define INTERNAL_SCL (22)
#define EXTERNAL_SDA (26)
#define EXTERNAL_SCL (27)

#define RF_CTS_IN (16) //for LTE-M

#ifdef OLD_DM  //*************
#define GPS_UART_TX_PORT (33)
#define GPS_UART_RX_PORT (39)
#define RF_WAKEUP_OUT (25)  //for LTE-M
#else //**********************
// for after V4
//#define RF_RESET_OUT (39) //for LTE-M, not use for V4
#define RF_WAKEUP_OUT (33)  //for LTE-M
#define RF_WAKE_INT_IN (25) //for LTE-M
#define EX_POWER_PORT (2)
#define RTC_FOUT_IN (35)    // for RTC

#ifndef DM_V4
// for after V5
#define EX_IRQ_IN (39)   // for IO-Expander interrupt
#define RF_RESET_OUT (4) // for LTE-M
#endif // DM_V4

#endif //********************

#define SD_CHIPSELECT (5)	//SD cs port

// Other settings
#define MLR_BAUDRATE        (19200)

#define BME280_I2C_ADD (0x76)  //Sensor
#define AD5693_I2C_ADD (0x4C)  //DA
#define MCP3425_I2C_ADD (0x68) //AD
#define ADP5589_I2C_ADD (0x34) //GPIO Expander
#define RX8130_I2C_ADD (0x32)  //RTC
#define MCP3221_I2C_ADD (0x4D) //AD

#define TRUE                    (1)
#define FALSE                   (0)
#define ON (1)
#define OFF (0)

//UART0 is for debug use
#define GPS_UART_NO 1   //UART1
#define EX_UART_NO 1    //UART1 choose GPS or external
#define MODEM_UART_NO 2 //UART2

#define I2C_INTERNAL_NO 0
#define I2C_EXTERNAL_NO 1

class DateTime
{
public:
    DateTime(uint16_t year, uint8_t month, uint8_t day,
             uint8_t hour = 0, uint8_t min = 0, uint8_t sec = 0);

    uint16_t year() const { return 2000 + y; }
    uint8_t month() const { return m; }
    uint8_t day() const { return d; }
    uint8_t hour() const { return hh; }
    uint8_t minute() const { return mm; }
    uint8_t second() const { return ss; }
    uint8_t dayOfWeek() const;

protected:
    uint8_t y, m, d, hh, mm, ss;
};

#endif // _BOARD_H_
