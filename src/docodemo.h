/*
 * Include header for DocodeModem
 *
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#pragma once
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/timers.h>
#include <freertos/queue.h>

#include "Board.h"
#include "rx8130.h"

#define SerialDebug Serial

class DOCODEMO{
public:
    DOCODEMO(void);
    bool begin();

    bool LedCtrl(uint8_t led, uint8_t onoff);
    bool ModemPowerCtrl(uint8_t onoff);
    bool GpsPowerCtrl(uint8_t onoff);
    bool BeepVolumeCtrl(uint8_t vol);
    bool setPwm(uint16_t val);

    uint8_t readDipSW();
    int readExIN(uint8_t port);
    bool exOutCtrl(uint8_t port, uint8_t onoff);
#ifndef OLD_DM
    bool exPowerCtrl(uint8_t onoff);
#endif
    float readPressure();
    float readHumidity();
    float readTemperature();
    
    float readExADC();
    bool setDAC(float volt);
    bool writeDAC(uint16_t val);
    bool DacDown();

    DateTime rtcNow();
    bool rtcAdjust(const DateTime &dt);
    bool setRtcTimer(const RtcTimerPeriod period, uint16_t count);
    bool stopRtcTimer(void);
    bool rtcValidAtBoot = false;
    bool rtcAdjusted = false;
	bool chkRtcBattery(void);

private:
    bool exGPIO_init(TwoWire &theWire);
};

extern DOCODEMO Dm;
