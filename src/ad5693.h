/*
 * DA converter driver
 * ad5693.h
 *
 * Copyright (c) 2020 Circuit Desgin,Inc
 * Released under the MIT license
 */
#ifndef _AD5693_H_
#define _AD5693_H_

#include "Board.h"

class AD5693
{
public:
    AD5693();
    bool begin(uint8_t addr = MCP3425_I2C_ADD, TwoWire &theWire = Wire, float Vref = 3.0);
    void setDAC(float volt);
    void Stop();

private:
    uint16_t readRegister16(uint8_t reg);
    void writeRegister16(uint8_t reg, uint16_t val);
    TwoWire *_wire;
    uint8_t i2c_addr;
    float _Vref;
};
#endif