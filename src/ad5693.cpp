/*
 * DA converter driver
 * ad5693.cpp
 *
 Original
 https://github.com/ControlEverythingCommunity/AD5693/blob/master/Arduino/AD5693.ino

 Distributed with a free-will license.
 Use it any way you want, profit or free, provided it fits in the licenses of its associated works.

 Modifyed by Circuit Desgin,Inc.
*/

#include "ad5693.h"

AD5693::AD5693(void)
{
}

void AD5693::writeRegister16(uint8_t reg, uint16_t val)
{
    _wire->beginTransmission(i2c_addr);
    _wire->write(reg); //set the register pointer
    _wire->write(val >> 8); //set the register pointer
    _wire->write(val & 0xff); //set the register pointer
    _wire->endTransmission();
}

uint16_t AD5693::readRegister16(uint8_t reg)
{
    uint16_t resp = 0;

    _wire->beginTransmission(i2c_addr);
    _wire->write(reg); //set the register pointer
    _wire->endTransmission(false);

    //read back the response
    uint8_t num_bytes = 2;
    _wire->requestFrom(i2c_addr, num_bytes);

    resp = _wire->read() << 8 | _wire->read();

    return resp;
}

bool AD5693::begin(uint8_t addr, TwoWire &theWire, float Vref)
{
    i2c_addr = addr;
    _wire = &theWire;
    _Vref = Vref;

    byte error;
    _wire->beginTransmission(i2c_addr);
    error = _wire->endTransmission();
    if (error != 0){
        printf("device not found(%02x)\n",addr);
        return false;
    }

    Stop();//power down for power save

    return true;
}

void AD5693::setDAC(float volt)
{
#ifdef OLD_DM
    uint32_t val = (uint32_t)((volt * 65535.0) / 5.0);
#else
    uint32_t val = (uint32_t)((volt * 65535.0) / _Vref);
#endif

    writeRegister16(0x40, 0x1000);// Normal mode,Internal ref off
    writeRegister16(0x30, (uint16_t)val);
}

void AD5693::Stop()
{
    writeRegister16(0x40, 0x7000);
}