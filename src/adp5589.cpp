/*
 * External GPIO driver 
 * ADP5589.cpp
 * 
 * Original
 * https://github.com/deanm1278/ADP5589_Arduino
 * Arduino library for the ADP5589 keypad controller
 * April 7, 2016 Dean Miller
 * MIT license
 * 
 * Modifyed by Circuit Desgin.
 */

#include "Board.h"
#include <Wire.h>
#include "ADP5589.h"

static const uint8_t gpio_dir_reg[] = {
    ADP5589_ADR_GPIO_DIRECTION_A,
    ADP5589_ADR_GPIO_DIRECTION_B,
    ADP5589_ADR_GPIO_DIRECTION_C
};

static const uint8_t gpo_data_reg[] = {
    ADP5589_ADR_GPO_DATA_OUT_A,
    ADP5589_ADR_GPO_DATA_OUT_B,
    ADP5589_ADR_GPO_DATA_OUT_C
};

static const uint8_t gpi_status_reg[] = {
    ADP5589_ADR_GPI_STATUS_A,
    ADP5589_ADR_GPI_STATUS_B,
    ADP5589_ADR_GPI_STATUS_C
};

ADP5589::ADP5589(void){
}

bool ADP5589::begin(uint8_t addr, TwoWire &theWire)
{
    i2c_addr = addr;
    _wire = &theWire;

    uint8_t _sensorID = readRegister(ADP5589_ADR_ID);
    if ((_sensorID & 0xf0) != 0x10)
        return false;
    
    //enable the internal oscillator, set to 50khz
    writeRegister(ADP5589_ADR_GENERAL_CFG_B, ADP5589_GENERAL_CFG_B_OSC_EN| 
                             ADP5589_GENERAL_CFG_B_CORE_FREQ(0));
    //Set function
    writeRegister(ADP5589_ADR_PIN_CONFIG_A,0);
    writeRegister(ADP5589_ADR_PIN_CONFIG_B,0);
    writeRegister(ADP5589_ADR_PIN_CONFIG_C,0);
    writeRegister(ADP5589_ADR_PIN_CONFIG_D, 0x08);

    //Init first value
    writeRegister(ADP5589_ADR_GPO_DATA_OUT_A, 3);
    writeRegister(ADP5589_ADR_GPO_DATA_OUT_B, 0);
    writeRegister(ADP5589_ADR_GPO_DATA_OUT_C, 0);

    //Init as input first
    writeRegister(ADP5589_ADR_GPIO_DIRECTION_A, 0);
    writeRegister(ADP5589_ADR_GPIO_DIRECTION_B, 0);
    writeRegister(ADP5589_ADR_GPIO_DIRECTION_C, 0);

    //PullUp Setting
    writeRegister(ADP5589_ADR_RPULL_CONFIG_A, 0x05);
    writeRegister(ADP5589_ADR_RPULL_CONFIG_B, 0xBf);
    writeRegister(ADP5589_ADR_RPULL_CONFIG_C, 0x55);
    writeRegister(ADP5589_ADR_RPULL_CONFIG_D, 0x55);
    writeRegister(ADP5589_ADR_RPULL_CONFIG_E, 0x3e);

    //Interrupt level
    writeRegister(ADP5589_ADR_GPI_INT_LEVEL_A, 0x80);
    writeRegister(ADP5589_ADR_GPI_INT_LEVEL_B, 0x00);
    writeRegister(ADP5589_ADR_GPI_INT_LEVEL_C, 0x01);

    //Interrupt Enable/Disable
    writeRegister(ADP5589_ADR_GPI_INTERRUPT_EN_A, 0x00);
    writeRegister(ADP5589_ADR_GPI_INTERRUPT_EN_B, 0x00);
    writeRegister(ADP5589_ADR_GPI_INTERRUPT_EN_C, 0x00);

    return true;
}

void ADP5589::gpioSetDirection(uint8_t gpio, uint8_t dir){
    if(gpio > 0 && gpio <= 19 && dir <= 1){
        uint8_t bit = ((gpio - 1) % 8);
        uint8_t reg = gpio_dir_reg[(gpio - 1) / 8];
        
        uint8_t toWrite = readRegister(reg);
        toWrite ^= (-dir ^ toWrite) & (1 << bit);
        
        writeRegister(reg, toWrite);
    }
}

void ADP5589::writeRegister(uint8_t reg, uint8_t val){
    //Write the specified value to the specified register
    _wire->beginTransmission(i2c_addr);
    _wire->write(reg); //set register pointer
    _wire->write(val); //set value
    _wire->endTransmission();
}

uint8_t ADP5589::readRegister(uint8_t reg)
{
    uint8_t resp = 0;

    _wire->beginTransmission(i2c_addr);
    _wire->write(reg); //set the register pointer
    _wire->endTransmission(false);

    //read back the response
    uint8_t num_bytes = 1;
    _wire->requestFrom(i2c_addr, num_bytes);

    resp = _wire->read();

    return resp;
}

//gpio is from 1 to 19, GPIO_1 = R0,GPIO_9 = C0
bool ADP5589::gpioWrite(uint8_t gpio, uint8_t val){
    //Write the specified value to the specified GPIO pin
    if(gpio > 0 && gpio <= 19 && val <= 1){
        uint8_t bit = ((gpio - 1) % 8);
        uint8_t reg = gpo_data_reg[(gpio - 1) / 8];
        
        uint8_t toWrite = readRegister(reg);
        toWrite ^= (-val ^ toWrite) & (1 << bit);
        
        writeRegister(reg, toWrite);
        return true;
    }else{
        return false;
    }
}

//gpio is from 1 to 19, GPIO_1 = R0,GPIO_9 = C0
int ADP5589::gpioRead(uint8_t gpio)
{
    //Read the specified value to the specified GPIO pin
    if (gpio > 0 && gpio <= 19)
    {
        uint8_t bit = ((gpio - 1) % 8);
        uint8_t reg = gpi_status_reg[(gpio - 1) / 8];

        uint8_t toWrite = readRegister(reg);
        return (toWrite & (1 << bit) ? 1 : 0);
    }else{
        return -1;
    }
}

